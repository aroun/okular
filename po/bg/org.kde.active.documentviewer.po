# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Yasen Pramatarov <yasen@lindeas.com>, 2013.
# Mincho Kondarev <mkondarev@yahoo.de>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-08-03 00:44+0000\n"
"PO-Revision-Date: 2022-08-03 09:46+0200\n"
"Last-Translator: Mincho Kondarev <mkondarev@yahoo.de>\n"
"Language-Team: Bulgarian <kde-i18n-doc@kde.org>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.04.3\n"

#: package/contents/ui/Bookmarks.qml:20 package/contents/ui/OkularDrawer.qml:85
msgid "Bookmarks"
msgstr "Отметки"

#: package/contents/ui/CertificateViewerDialog.qml:21
msgid "Certificate Viewer"
msgstr "Разглеждане на сертификати"

#: package/contents/ui/CertificateViewerDialog.qml:32
msgid "Issued By"
msgstr "Издаден от"

#: package/contents/ui/CertificateViewerDialog.qml:37
#: package/contents/ui/CertificateViewerDialog.qml:65
msgid "Common Name:"
msgstr "Общо име:"

#: package/contents/ui/CertificateViewerDialog.qml:43
#: package/contents/ui/CertificateViewerDialog.qml:71
msgid "EMail:"
msgstr "Е-поща:"

#: package/contents/ui/CertificateViewerDialog.qml:49
#: package/contents/ui/CertificateViewerDialog.qml:77
msgid "Organization:"
msgstr "Организация:"

#: package/contents/ui/CertificateViewerDialog.qml:60
msgid "Issued To"
msgstr "Издаден на"

#: package/contents/ui/CertificateViewerDialog.qml:88
msgid "Validity"
msgstr "Валидност"

#: package/contents/ui/CertificateViewerDialog.qml:93
msgid "Issued On:"
msgstr "Издаден на:"

#: package/contents/ui/CertificateViewerDialog.qml:99
msgid "Expires On:"
msgstr "Изтича:"

#: package/contents/ui/CertificateViewerDialog.qml:110
msgid "Fingerprints"
msgstr "Отпечатъци"

#: package/contents/ui/CertificateViewerDialog.qml:115
msgid "SHA-1 Fingerprint:"
msgstr "SHA-1 Fingerprint:"

#: package/contents/ui/CertificateViewerDialog.qml:121
msgid "SHA-256 Fingerprint:"
msgstr "SHA-256 Fingerprint:"

#: package/contents/ui/CertificateViewerDialog.qml:135
msgid "Export..."
msgstr "Експортиране..."

#: package/contents/ui/CertificateViewerDialog.qml:141
#: package/contents/ui/SignaturePropertiesDialog.qml:148
msgid "Close"
msgstr "&Затваряне"

#: package/contents/ui/CertificateViewerDialog.qml:149
msgid "Certificate File (*.cer)"
msgstr "Сертификатен файл (* .cer)"

#: package/contents/ui/CertificateViewerDialog.qml:164
#: package/contents/ui/SignaturePropertiesDialog.qml:168
msgid "Error"
msgstr "Грешка"

#: package/contents/ui/CertificateViewerDialog.qml:166
msgid "Could not export the certificate."
msgstr "Сертификатът не можа да се експортира."

#: package/contents/ui/main.qml:23 package/contents/ui/main.qml:64
msgid "Okular"
msgstr "Okular"

#: package/contents/ui/main.qml:40
msgid "Open..."
msgstr "Отваряне..."

#: package/contents/ui/main.qml:47
msgid "About"
msgstr "За програмата"

#: package/contents/ui/main.qml:104
msgid "Password Needed"
msgstr "Необходима е парола"

#: package/contents/ui/MainView.qml:25
msgid "Remove bookmark"
msgstr "Премахване на отметка"

#: package/contents/ui/MainView.qml:25
msgid "Bookmark this page"
msgstr "Отбелязване на страницата"

#: package/contents/ui/MainView.qml:82
msgid "No document open"
msgstr "Няма документ за отваряне"

#: package/contents/ui/OkularDrawer.qml:57
msgid "Thumbnails"
msgstr "Миниатюри"

#: package/contents/ui/OkularDrawer.qml:71
msgid "Table of contents"
msgstr "Съдържание"

#: package/contents/ui/OkularDrawer.qml:99
msgid "Signatures"
msgstr "Подпис"

#: package/contents/ui/SignaturePropertiesDialog.qml:30
msgid "Signature Properties"
msgstr "Свойства на подписа"

#: package/contents/ui/SignaturePropertiesDialog.qml:44
msgid "Validity Status"
msgstr "Състояние на валидност"

#: package/contents/ui/SignaturePropertiesDialog.qml:50
msgid "Signature Validity:"
msgstr "Валидност на подписите:"

#: package/contents/ui/SignaturePropertiesDialog.qml:56
msgid "Document Modifications:"
msgstr "Модификации на документа:"

#: package/contents/ui/SignaturePropertiesDialog.qml:63
msgid "Additional Information"
msgstr "Допълнителна информация"

#: package/contents/ui/SignaturePropertiesDialog.qml:72
msgid "Signed By:"
msgstr "Подписано от:"

#: package/contents/ui/SignaturePropertiesDialog.qml:78
msgid "Signing Time:"
msgstr "Време за подписване:"

#: package/contents/ui/SignaturePropertiesDialog.qml:84
msgid "Reason:"
msgstr "Причина:"

#: package/contents/ui/SignaturePropertiesDialog.qml:91
msgid "Location:"
msgstr "Адрес:"

#: package/contents/ui/SignaturePropertiesDialog.qml:100
msgid "Document Version"
msgstr "Версия на документа"

#: package/contents/ui/SignaturePropertiesDialog.qml:110
msgctxt "Document Revision <current> of <total>"
msgid "Document Revision %1 of %2"
msgstr "Ревизия на документ %1 от %2"

#: package/contents/ui/SignaturePropertiesDialog.qml:114
msgid "Save Signed Version..."
msgstr "Запазване на подписаната версия..."

#: package/contents/ui/SignaturePropertiesDialog.qml:128
msgid "View Certificate..."
msgstr "Преглед на сертификата..."

#: package/contents/ui/SignaturePropertiesDialog.qml:170
msgid "Could not save the signature."
msgstr "Подписът не можа да бъде запазен."

#: package/contents/ui/Signatures.qml:27
msgid "Not Available"
msgstr "Не е наличен"

#: package/contents/ui/ThumbnailsBase.qml:43
msgid "No results found."
msgstr "Не са открити резултати."
