msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-03-10 00:43+0000\n"
"PO-Revision-Date: 2022-11-06 01:06\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/okular/okular_epub.pot\n"
"X-Crowdin-File-ID: 5790\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "KDE 中国,Ni Hui"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "kde-china@kde.org,shuizhuyuanluo@126.com"

#: converter.cpp:163
#, kde-format
msgid "Error while opening the EPub document."
msgstr "打开 EPub 文档时出错。"

#: generator_epub.cpp:30
#, kde-format
msgid "EPub"
msgstr "EPub"

#: generator_epub.cpp:30
#, kde-format
msgid "EPub Backend Configuration"
msgstr "EPub 后端配置"
